package com.getmethemenu;

 

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.ByteArrayBuffer;

import android.content.Context;
import android.net.ParseException;
import android.util.Log;

public class Http
{

	private static String url = "http://97.74.200.136/samepinchapp/";//"http://www.mobilespylogs.com/webapi/";

	private static InputStream OpenHttpConnection(String urlString) throws IOException
	{
		InputStream in = null;
		int response = -1;

		URL url = new URL(urlString); 
		URLConnection conn = url.openConnection();

		if (!(conn instanceof HttpURLConnection))                     
			throw new IOException("Not an HTTP connection");

		try
		{
				HttpURLConnection httpConn = (HttpURLConnection) conn;
				httpConn.setAllowUserInteraction(false);
				httpConn.setInstanceFollowRedirects(true);
				httpConn.setRequestMethod("GET");
				httpConn.connect(); 
	
				response = httpConn.getResponseCode(); 
				//Log.i("CONNNNNNNNNNEECRESPONSE ",""+response);
				
				
				if (response == HttpURLConnection.HTTP_OK) 
				{
						//Log.i("CONNNNNNNNNNEECRESPONSE ","isOK");
						in = httpConn.getInputStream(); 
				} 
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			throw new IOException("Error connecting");            
		}
		return in;     
	}


	public static String http_get(String urlParameters)
	{ 
		
		String url1 =  urlParameters ;
		
		Log.i("THIS IS URL ",url1);

		int BUFFER_SIZE = 2000;
		InputStream in = null;
		try 
		{
				in = OpenHttpConnection(url1);
		} 
		catch (IOException e1) 
		{
				e1.printStackTrace();
		}

		try
		{
			InputStreamReader isr = new InputStreamReader(in);
			int charRead;
			String str = "";
			char[] inputBuffer = new char[BUFFER_SIZE];          
			try 
			{
				while ((charRead = isr.read(inputBuffer))>0)
				{                    
					String readString = String.copyValueOf(inputBuffer, 0, charRead);                    
					str += readString;
					inputBuffer = new char[BUFFER_SIZE];
				}
				in.close();
			} 
			catch (IOException e) 
			{
					e.printStackTrace();
					return "";
			}   
			
			//Log.i("Server responsessss ",str);
			
			if(str.indexOf("1_")!=-1)
			{
					return str;
			}
					
			return str;
		}
		catch (Exception e)
		{
				e.printStackTrace();	
		}
		
		return "";
	}
	
	
	
	public static String http_get_place_to_coordinate(String placename)
	{
		//String uid = "";
		
		String url1 = "http://maps.google.com/maps/geo?q="+placename+"&output=csv" ;

		int BUFFER_SIZE = 2000;
		InputStream in = null;
		try 
		{
				in = OpenHttpConnection(url1);
		} 
		catch (IOException e1) 
		{
				e1.printStackTrace();
		}

		try
		{
			InputStreamReader isr = new InputStreamReader(in);
			int charRead;
			String str = "";
			char[] inputBuffer = new char[BUFFER_SIZE];          
			try 
			{
				while ((charRead = isr.read(inputBuffer))>0)
				{                    
					String readString = String.copyValueOf(inputBuffer, 0, charRead);                    
					str += readString;
					inputBuffer = new char[BUFFER_SIZE];
				}
				in.close();
			} 
			catch (IOException e) 
			{
					e.printStackTrace();
					return "";
			}   
			
			//Log.i("Server responsessss ",str);
			
			if(str.indexOf("1_")!=-1)
			{
					return str;
			}
					
			return str;
		}
		catch (Exception e)
		{
				e.printStackTrace();	
		}
		
		return "";
	}
	
	
	
	
	
	
	
	
	
	
	public static String http_get_gps(String urlParameters)
	{
		String uid = "";
		
		String url1 =  url+ urlParameters ;
		
		Log.e("URL",url1);

		int BUFFER_SIZE = 2000;
		InputStream in = null;
		try 
		{
				in = OpenHttpConnection(url1);
		} 
		catch (IOException e1) 
		{
				e1.printStackTrace();
		}

		try
		{
			InputStreamReader isr = new InputStreamReader(in);
			int charRead;
			String str = "";
			char[] inputBuffer = new char[BUFFER_SIZE];          
			try 
			{
				while ((charRead = isr.read(inputBuffer))>0)
				{                    
					String readString = String.copyValueOf(inputBuffer, 0, charRead);                    
					str += readString;
					inputBuffer = new char[BUFFER_SIZE];
				}
				in.close();
			} 
			catch (IOException e) 
			{
					e.printStackTrace();
					return "";
			}   
			
			//Log.i("Server responsessss ",str);
			
			if(str.indexOf("1_")!=-1)
			{
					return str;
			}
					
			return str;
		}
		catch (Exception e)
		{
				e.printStackTrace();	
		}
		
		return "";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String handleCommand(String urlParameters)
	{
		String uid = "";
		
		String url1 = url + urlParameters ;
		
		//Log.i("WEEEEEEBCOMAMMANSD Process ",url1);
		
		int BUFFER_SIZE = 2000;
		InputStream in = null;
		try 
		{
				in = OpenHttpConnection(url1);
		} 
		catch (IOException e1) 
		{
				e1.printStackTrace();
				return "-1";
		}
		
		
		try
		{
			
			//InputStreamReader isr = new InputStreamReader(in);
			InputStream isr=in;
			int charRead;
			String str = "";
			byte[] inputBuffer = new byte[BUFFER_SIZE];          
			try 
			{
				while ((charRead = isr.read(inputBuffer))>0)
				{                    
					String readString = new String(inputBuffer, 0, charRead);                    
					str += readString;
					inputBuffer = new byte[BUFFER_SIZE];
				}
				in.close();
			} 
			catch (IOException e) 
			{
					e.printStackTrace();
					return "";
			}   
			
			//Log.i("Server responsessss ",str);
			
			if(str.indexOf("1_")!=-1)
			{
					return str;
			}
					
			return str;
		}
		catch (Exception e)
		{
				e.printStackTrace();	
		}
		
		return "";
	}
	public static String postData(String user,String file, String Contents)
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+file;
		
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("username", user));   
			nameValuePairs.add(new BasicNameValuePair("password", Contents));  
			nameValuePairs.add(new BasicNameValuePair("longitude", Contents));  
			nameValuePairs.add(new BasicNameValuePair("latitude", Contents));  
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	public static String postgpsData(String uid, String longitude,String latitude,String file )
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+file;
		
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("uid", uid));   
			nameValuePairs.add(new BasicNameValuePair("longitude", longitude));  
			nameValuePairs.add(new BasicNameValuePair("latitude", latitude));  
			  
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	
	public static String postLogin(String username, String password,String file )
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+file;
		
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("username", username));   
			nameValuePairs.add(new BasicNameValuePair("password", password));  
			nameValuePairs.add(new BasicNameValuePair("devicetype", "AN")); 
			   
			  
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	
	public static String getnearbyusers(String uid, String longitude,String latitude , String distance,String file )
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+file;
		
		
		Log.e("URL REQUESTED",newUrl);
		Log.e("DISTANCE SENT",distance);
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("uid", uid));   
			nameValuePairs.add(new BasicNameValuePair("longitude", longitude)); 
			nameValuePairs.add(new BasicNameValuePair("latitude", latitude)); 
			nameValuePairs.add(new BasicNameValuePair("distance", distance)); 
			   
			  
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	public static String signup_no_photo(String username, String password,String email  )
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+"registration.php";
		
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("username", username));   
			nameValuePairs.add(new BasicNameValuePair("password", password)); 
			nameValuePairs.add(new BasicNameValuePair("devicetype", "AN")); 
			nameValuePairs.add(new BasicNameValuePair("email", email)); 
			   
			  
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	
	public static String editprofile_no_photo(String uid, String devicetype,String email , String description , String firstname,String lastname,String dob,String address,String gender,String facebookid ,String link )
	{   

		HttpClient httpclient = new DefaultHttpClient();  
		String newUrl="";
		
		newUrl=url+"editprofile.php";
		
		HttpPost httppost = new HttpPost(newUrl);   
		String result="";

		try 
		{   
			   
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
			
			nameValuePairs.add(new BasicNameValuePair("uid", uid));   
		   nameValuePairs.add(new BasicNameValuePair("devicetype", "AN")); 
		   if(email!=null)
			nameValuePairs.add(new BasicNameValuePair("email", email)); 
		   if(description!=null)
				nameValuePairs.add(new BasicNameValuePair("description", description)); 
			   
		   if(firstname!=null)
				nameValuePairs.add(new BasicNameValuePair("firstname", firstname)); 
			  
		   if(lastname!=null)
				nameValuePairs.add(new BasicNameValuePair("lastname", lastname)); 
		   
		   if(dob!=null)
				nameValuePairs.add(new BasicNameValuePair("dob", dob)); 
		   

		   if(address!=null)
				nameValuePairs.add(new BasicNameValuePair("address", address)); 
		   
		   if(gender!=null)
				nameValuePairs.add(new BasicNameValuePair("gender", gender)); 
		   
		   if(facebookid==null)
			   facebookid="0";
		   if(link==null)
			   link = "0";
		   
				nameValuePairs.add(new BasicNameValuePair("facebookid", facebookid)); 
				nameValuePairs.add(new BasicNameValuePair("link_facebook", link)); 
				Log.i("LINK FACEBOOK FROM HTTP CLASS",link);
		   
		   
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));   

			HttpResponse response = httpclient.execute(httppost); 

			InputStream is = response.getEntity().getContent(); 
			BufferedInputStream bis = new BufferedInputStream(is); 
			ByteArrayBuffer baf = new ByteArrayBuffer(20); 

			int current = 0;   
			while((current = bis.read()) != -1)
			{   
				baf.append((byte)current);   
			}   
			result=new String(baf.toByteArray());


		} 
		catch (ClientProtocolException e) 
		{   
		} 
		catch (Exception e) 
		{   
		} 
		
		
		 
		return result;
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static String uploadFile(String urlString,File exsistingFileName,Context context)// ,	String date,String time)
	{

	/*	System.out.println("exsistingFileName="+exsistingFileName);
		System.out.println("date="+date);
		System.out.println("time="+time);
		System.out.println("user ID="+user);
		System.out.println("username="+username);*/
		
      
		String lineEnd = "\r\n"; 
		String twoHyphens = "--"; 
		String boundary = "*****"; 
		//String urlString="http://pccellcontrol.com/nemat/upload.php"; 
	//	String urlString=url+"registration.php"; 
		HttpURLConnection conn; 
		DataOutputStream dos;
		//String temp1=exsistingFileName.substring(0,exsistingFileName.indexOf("."));
		//String temp2=exsistingFileName.substring(exsistingFileName.indexOf("."));
		//String fileName=temp1.substring(0,temp1.length()-1)+temp2;
		
		
		String fileName=exsistingFileName.getAbsolutePath().substring(exsistingFileName.getAbsolutePath().lastIndexOf("/")+1);
		fileName=fileName.replaceAll("_","-");
		
		////Log.i("#######file to upload-->",fileName);
		
		
		Hashtable params = new Hashtable();
		params.put("type", "Image");
		 
       
        //params.put("file", "file");
        //params.put("filename", fileName);
		String response="";

		try
		{ 


			FileInputStream fileInputStream  =new FileInputStream(exsistingFileName); //context.openFileInput(exsistingFileName );
		
			int bytesAvailable = fileInputStream.available(); 
			
			
			if(bytesAvailable/(1024*1024)<=2)
			{
					
			URL url = new URL(urlString); 


			// Open a HTTP connection to the URL 

			conn = (HttpURLConnection) url.openConnection(); 

			// Allow Inputs 
			conn.setDoInput(true); 

			// Allow Outputs 
			conn.setDoOutput(true); 

			// Don't use a cached copy. 
			conn.setUseCaches(false); 

			// Use a post method. 
			conn.setRequestMethod("POST"); 

			conn.setRequestProperty("Connection", "Keep-Alive"); 

			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary); 
			
			//conn.setRequestProperty("Transfer-Encoding","chunked");	
			
		//	conn.setRequestProperty("User-Agent", "Android");
            conn.setRequestProperty("Content-Language", "en-US");
           

			dos = new DataOutputStream( conn.getOutputStream() ); 

			//dos.writeBytes(twoHyphens + boundary + lineEnd); 
			//dos.writeBytes(lineEnd); 
		//	dos.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + fileName +"\"" + lineEnd+";date=\""+date+";time=\""+time+";sID=\""+user +"\"" + lineEnd);
			//dos.writeBytes("Content-Disposition: form-data"+";username=\""+username +";sID=\""+user+"\""+";name=\"file\";filename=\"" + fileName +"\"" + lineEnd+ ";Content-Type:image/jpeg" +"\"" + lineEnd); 
			////dos.writeBytes(getBoundaryMessage(boundary,params,"file", fileName,"image/jpeg"));
			//dos.writeBytes("Content-Disposition: form-data; name=\"file\";"
			  //			+ " filename=\"" + fileName +"\"" + lineEnd);
			///dos.writeBytes(lineEnd); 
			dos.writeBytes(getBoundaryMessage1(boundary,params,"image", fileName,"image/jpg"));
		//	dos.writeBytes("Content-Disposition: form-data; name=\"file\";"
			//   			+ " filename=\"" + fileName +"\"" + lineEnd);
			//dos.writeBytes(lineEnd); 
			
			//Log.e("Tag","Headers are written"); 

			// create a buffer of maximum size 
			int maxBufferSize=1024; 
			
		//	Log.e("-----------bytesAvailable",""+bytesAvailable); 
			int bufferSize = Math.min(bytesAvailable, maxBufferSize); 
			byte[] buffer = new byte[bufferSize]; 

			// read file and write it into form... 

			int bytesRead = fileInputStream.read(buffer, 0, bufferSize); 
			System.out.println("read file and write it into form="+bytesAvailable);
			while (bytesRead > 0) 
			{ 
				dos.write(buffer, 0, bufferSize); 
				bytesAvailable = fileInputStream.available(); 
				bufferSize = Math.min(bytesAvailable, maxBufferSize); 
				bytesRead = fileInputStream.read(buffer, 0, bufferSize); 
				//System.out.println("send multipart form data necesssary after file data="+bytesRead);
			} 


			// send multipart form data necesssary after file data... 

			dos.writeBytes(lineEnd); 
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd); 

			// close streams 
		//	Log.e("Tag","File is written"); 
			fileInputStream.close(); 
			dos.flush(); 
			InputStream is = conn.getInputStream(); 
			//   retrieve the response from server 
			int ch; 

			StringBuffer b =new StringBuffer(); 
			while( ( ch = is.read() ) != -1 ){ 
				b.append( (char)ch ); 
			} 
			response=b.toString();     
			
	//	Log.i("-------------Response",response); 
			dos.close(); 

			//  dos.close(); 

			}
			else
			{
				response="1_NotSent";
			}


		}
		catch(OutOfMemoryError error)
		{
			System.gc();
		}
		catch(Exception e)
		{
			System.out.println("this is error"+e.toString());
		//	response=e.toString();
		}

		return response;

	}
	
	public static String uploadFileByData(String urlString,String data,Context context)
	{
		String lineEnd = "\r\n"; 
		String twoHyphens = "--"; 
		String boundary = "*****"; 
		
		// String urlString=url+"uploadXML.php"; 
		HttpURLConnection conn; 
		DataOutputStream dos;
				
		DateFormat df=new SimpleDateFormat("MMddyyhhmmss");
        String cDT = df.format(System.currentTimeMillis());
        
		String fileName="rec"+cDT+".xml";
		
		//Log.i("FILENNNNNNNNNNNNNNAME->",fileName);
		
		Hashtable params = new Hashtable();
		params.put("type", "xml");
	//	params.put("username", username);
     //   params.put("userid",user);
        //params.put("file", "file");
        //params.put("filename", fileName);
		String response="";

		try
		{ 

			URL url = new URL(urlString); 
			
			// Open a HTTP connection to the URL 
			conn = (HttpURLConnection) url.openConnection(); 

			// Allow Inputs 
			conn.setDoInput(true); 

			// Allow Outputs 
			conn.setDoOutput(true); 

			// Don't use a cached copy. 
			conn.setUseCaches(false); 

			// Use a post method. 
			conn.setRequestMethod("POST"); 

			conn.setRequestProperty("Connection", "Keep-Alive"); 

			conn.setRequestProperty("Content-Type", "multipart/form-data;boundary="+boundary); 
			
			//conn.setRequestProperty("Transfer-Encoding","chunked");	
			
		//	conn.setRequestProperty("User-Agent", "Android");
            conn.setRequestProperty("Content-Language", "en-US");
           

			dos = new DataOutputStream( conn.getOutputStream() ); 

			dos.writeBytes(getBoundaryMessage1(boundary,params,"file", fileName,"image/jpg"));
		
			dos.write(data.getBytes()); 
		
			dos.writeBytes(lineEnd); 
			dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd); 

			dos.flush(); 
			
			InputStream is = conn.getInputStream(); 
			//   retrieve the response from server 
			
			int ch; 

			StringBuffer b =new StringBuffer(); 
			while( ( ch = is.read() ) != -1 ){ 
				b.append( (char)ch ); 
			} 
			response=b.toString();         
			
			//Log.i("XMLFILE-------------Response",response);
			//Log.i("XMLFILE-------------Response",response);
			//Log.i("XMLFILE-------------Response",response);
			dos.close(); 

		}
		catch(OutOfMemoryError error)
		{
			System.gc();
		}
		catch(Exception e)
		{
			System.out.println("this is error"+e.toString());
			response=e.toString();
		}

		return response;

	}
	
	private static String getBoundaryMessage1(String boundary, Hashtable params, String fileField, String fileName, String fileType) 
	{
        StringBuffer res = new StringBuffer("--").append(boundary).append("\r\n");
        Enumeration keys = params.keys();
        while(keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                String value = (String)params.get(key);
                res.append("Content-Disposition: form-data; name=\"").append(key).append("\"\r\n").append("\r\n").append(value).append("\r\n").append("--").append(boundary).append("\r\n");
        }
        res.append("Content-Disposition: form-data; name=\"").append(fileField).append("\"; filename=\"").append(fileName).append("\"\r\n").append("Content-Type: ").append(fileType).append("\r\n\r\n");
        return res.toString();
	}
	
	public static String getBoundaryMessage(String boundary, Hashtable params, String fileField, String fileName, String fileType)
    {
            StringBuffer res = new StringBuffer("--").append(boundary).append("\r\n");
            Enumeration keys = params.keys();

            while(keys.hasMoreElements())
            {
                    String key = (String)keys.nextElement();
                    String value = (String)params.get(key);
                    
                    System.out.println("KEY : " +key + ":" + "VALUE" +value);
                    
                    res.append("Content-Disposition: form-data; name=\"")
                        .append(key).append("\"\r\n").append("\r\n").append(value)
                        .append("\r\n").append("--").append(boundary).append("\r\n");
            }

         //   res.append("Content-Disposition: form-data; name=\"").append(fileField).append("\"; filename=\"").append(fileName).append("\"\r\n") 
         //   .append("Content-Type: ").append(fileType).append("\r\n\r\n");
            
        return res.toString();
    }
	
	
	public String multipartRequest(String urlTo, String post, String filepath, String filefield) throws ParseException, IOException {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;
        
        String twoHyphens = "--";
        String boundary =  "*****"+Long.toString(System.currentTimeMillis())+"*****";
        String lineEnd = "\r\n";
        
        String result = "";
        
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1*1024*1024;
        
        String[] q = filepath.split("/");
        int idx = q.length - 1;
        
        try {
                File file = new File(filepath);
                FileInputStream fileInputStream = new FileInputStream(file);
                
                URL url = new URL(urlTo);
                connection = (HttpURLConnection) url.openConnection();
                
                connection.setDoInput(true);
                connection.setDoOutput(true);
                connection.setUseCaches(false);
                
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Connection", "Keep-Alive");
                connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
                connection.setRequestProperty("Content-Type", "multipart/form-data; boundary="+boundary);
                
                outputStream = new DataOutputStream(connection.getOutputStream());
                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + filefield + "\"; filename=\"" + q[idx] +"\"" + lineEnd);
                outputStream.writeBytes("Content-Type: image/jpeg" + lineEnd);
                outputStream.writeBytes("Content-Transfer-Encoding: binary" + lineEnd);
                outputStream.writeBytes(lineEnd);
                
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while(bytesRead > 0) {
                        outputStream.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }
                
                outputStream.writeBytes(lineEnd);
                
                // Upload POST Data
                String[] posts = post.split("&");
                int max = posts.length;
                for(int i=0; i<max;i++) {
                        outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                        String[] kv = posts[i].split("=");
                        outputStream.writeBytes("Content-Disposition: form-data; name=\"" + kv[0] + "\"" + lineEnd);
                        outputStream.writeBytes("Content-Type: text/plain"+lineEnd);
                        outputStream.writeBytes(lineEnd);
                        outputStream.writeBytes(kv[1]);
                        outputStream.writeBytes(lineEnd);
                }
                
                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                
                inputStream = connection.getInputStream();
                result = this.convertStreamToString(inputStream);
                
                fileInputStream.close();
                inputStream.close();
                outputStream.flush();
                outputStream.close();
                
                return result;
        } catch(Exception e) {
                Log.e("MultipartRequest","Multipart Form Upload Error");
                e.printStackTrace();
                return "error";
        }
}

private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
                while ((line = reader.readLine()) != null) {
                        sb.append(line);
                }
        } catch (IOException e) {
                e.printStackTrace();
        } finally {
                try {
                        is.close();
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }
        return sb.toString();
}


}

