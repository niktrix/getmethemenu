package com.getmethemenu;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.android.AsyncFacebookRunner;
import com.facebook.android.BaseRequestListener;
import com.facebook.android.DialogError;
import com.facebook.android.Facebook;
import com.facebook.android.FacebookError;
import com.facebook.android.SessionStore;
import com.facebook.android.Util;
import com.facebook.android.Facebook.DialogListener;
 

 
 

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.Settings;
import android.provider.ContactsContract.CommonDataKinds.Im;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;

import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

@SuppressLint("ParserError")
public class MainActivity extends Activity {

	private WebView webView;
	private ProgressDialog waitingfordata;
	String URL = "";
	String BASE_API_URL = "http://www.getmethemenu.ca/app/";
	WebSettings webSettings;
	private Uri fileUri;
   String ImageTitle;
	protected boolean _taken = true;
	File sdImageMainDirectory;
	ImageView iv;
	final int TAKE_PICTURE = 1;
	final int SCAN_BAR_CODE = 2;
	protected static final String PHOTO_TAKEN = "photo_taken";

	Location currentLocation;
	static double currentLatitude = 0;
	static double currentLongitude = 0;

	String resid;
	String userid;
	
	Handler handler;
	ProgressBar mProgressBar;
	Facebook mFacebook;
	private SharedPreferences mPrefs;
	private static final String[] PERMS = new String[] {  "email" ,"user_birthday","status_update","publish_stream","read_friendlists"};
	//status_update,publish_stream,read_friendlists
	private AsyncFacebookRunner mAsyncRunner;
	String FB_APP_ID= "207553829311808";
	 String email;
	  
	    String ISLOGIN= "islogin";
	    String TRUE = "true";
	    String FALSE = "false";
	    SharedPreference sp;
	 
	public void onCreate(Bundle savedInstanceState) {
	
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// iv =(ImageView)findViewById(R.id.iv);
		
		sp = new SharedPreference(); // shared preference class 
	//	_this = this;
		

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.getmethemenu", PackageManager.GET_SIGNATURES);
			for (android.content.pm.Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.i("PXR", com.getmethemenu.Base64.encodeBytes(md.digest()));
			}
		} catch (NameNotFoundException e) {
		} catch (NoSuchAlgorithmException e) {
		}
		 

		mPrefs = getPreferences(MODE_PRIVATE);
		String access_token = mPrefs.getString("access_token", null);
		long expires = mPrefs.getLong("access_expires", 0);
		if (access_token != null) {
			mFacebook.setAccessToken(access_token);
		}
		if (expires != 0) {
			mFacebook.setAccessExpires(expires);
		}
		
		mFacebook = new Facebook(FB_APP_ID);

		mAsyncRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, this);
		
		
		handler = new Handler(){

			@Override
			public void dispatchMessage(Message msg) {
				// TODO Auto-generated method stub
				super.dispatchMessage(msg);
				if(msg.what==0)
				waitingfordata.dismiss();
				
				if(msg.what==1)
				{
					
					mProgressBar.setVisibility(ProgressBar.GONE);
				}
				if(msg.what==2)
				{
					SharedPreference.putData(getApplicationContext(), ISLOGIN, TRUE);
					Log.i("PXR",BASE_API_URL+"login?email="+email+"&access_token="+mFacebook.getAccessToken());
					webView.loadUrl( "http://www.getmethemenu.ca/appui/"+"login?email="+email+"&access_token="+mFacebook.getAccessToken());
					mProgressBar.setVisibility(ProgressBar.GONE);
				}
				
			}
			
			
		};
		FindLocation();
		if(SharedPreference.getData(getApplicationContext(),ISLOGIN).equals(TRUE))
			URL = "http://www.getmethemenu.ca/appui/"+"login?email="+email+"&access_token="+mFacebook.getAccessToken();
		else
		URL = "http://www.getmethemenu.ca/appui?lat=" + currentLatitude
				+ "&lng=" + currentLongitude + "&devicetoken=" + getDeviceId()
				+ "&os=android&osversion=" + android.os.Build.VERSION.RELEASE
				+ "";

		Log.e(URL, "deviceid");
		// Toast.makeText(getApplicationContext(), URL+"",
		// Toast.LENGTH_LONG).show();
		
		mProgressBar = (ProgressBar)findViewById(R.id.progressbar);
		webView = (WebView) findViewById(R.id.webView1);

		webSettings = webView.getSettings();
		webSettings.supportMultipleWindows();
		webView.setWebViewClient(new WebViewClient());
		webView.getSettings().setJavaScriptEnabled(true);
		webView.addJavascriptInterface(new JavaScriptInterface(this),
				"MyAndroid");

		webView.setWebChromeClient(new WebChromeClient() {
			
            public void onProgressChanged(WebView view, int progress) 
            {
            	 
            			 
            if(progress < 100 &&  mProgressBar.getVisibility()==ProgressBar.GONE){
            
            	mProgressBar.setVisibility(ProgressBar.VISIBLE);
              //  txtview.setVisibility(View.VISIBLE);
            }
            
            if(progress == 100) {
               handler.sendEmptyMessage(1);
             //   txtview.setVisibility(View.GONE);
            }
         }
     });
		webView.loadUrl(URL);

		// webView.loadUrl("javascript:testEcho('Hello World!')");

	}

	// mWebView.loadUrl("javascript:(function () { " + "testEcho(Hello World!);"
	// + "})()");

	private Uri selectedImageUri;

	String selectedImagePath;
	String imagePath;
	InputStream inputStream;

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		Log.e("On Activity Result", "On activity result");
		switch (requestCode) {
		case TAKE_PICTURE:
		{
			if (resultCode == Activity.RESULT_OK) {
				Bundle extras = data.getExtras();
				Bitmap b = (Bitmap) extras.get("data");
				// iv.setImageBitmap(b) ;

				Log.e("Bitmap height", b.getHeight() + "");
			 
				
				final AlertDialog.Builder alert = new AlertDialog.Builder(this);

				alert.setTitle("Enter Image Title");
//			/	alert.setMessage("Message");
                alert.setCancelable(false);
				// Set an EditText view to get user input 
				final EditText input = new EditText(this);
				alert.setView(input);

				alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				   ImageTitle = input.getEditableText().toString();
				  // Do something with value!
				   
			mProgressBar.setVisibility(ProgressBar.VISIBLE);
				   new Thread(){
						
						public void run(){
							
							
							File tempDir = null;
							try {
								if (Environment.getExternalStorageState().equals(
										Environment.MEDIA_MOUNTED)) {
									File externalRoot = Environment
											.getExternalStorageDirectory();
									tempDir = new File(externalRoot, "temp.jpg");
									sendPost("", tempDir);
									
								handler.sendEmptyMessage(1);
									 

								}

							} catch (Exception e) {
								e.printStackTrace();
							//	Log.e("Fail", "Fail");
								
							}
							
						}
						
					}.start();  
				  
				  
				  }
				});

			 

				alert.show();
				
					
					 
			

			}
		}
		
		break;

		case SCAN_BAR_CODE: {
			if (resultCode == RESULT_OK) {
				final String contents = data.getStringExtra("SCAN_RESULT");
				String format = data.getStringExtra("SCAN_RESULT_FORMAT");
				// Handle successful scan

				// http://www.getmethemenu.ca/app/usecoupon?resid=2&cpnid=8&userid=2&code=abcdef&billamt=10
				Toast.makeText(getApplicationContext(), contents + "",
						Toast.LENGTH_LONG).show();
				 waitingfordata = ProgressDialog.show(MainActivity.this, "", "Checking In...", true,
                         false);
				 
				 
				Log.e("FORMAT", format);
				
  new Thread(){
					  
					  public void run()
					  {
						  
						 
				Log.e("",	Http.http_get(BASE_API_URL+"usecoupon?resid=2&cpnid=8&userid=2&code="+contents+"&billamt=10"));
									handler.sendEmptyMessage(0);

								

						 
					  }
					  
				  }.start();
			} 
		}
		}

	}

	public class JavaScriptInterface {
		Context mContext;

		JavaScriptInterface(Context c) {
			mContext = c;
		}

		// add other interface methods to be called from JavaScript
		public void picture(String res, String user, String lat, String longi) {
			 try
			 {
			 currentLatitude = Double.parseDouble(lat);
			 currentLongitude = Double.parseDouble(longi);
			 }catch(NumberFormatException e)
			 {}
			 userid = user;
			 resid = res;
			 Intent cameraIntent = new
			 Intent("android.media.action.IMAGE_CAPTURE");
			
			
			
			 startActivityForResult(cameraIntent, TAKE_PICTURE);
//
//			Intent intent = new Intent("com.google.zxing.client.android.SCAN");
//
//			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");
//
//			startActivityForResult(intent, SCAN_BAR_CODE);
		}

		public void scanbarcode(String resid, String userid, String lat,
				String lng, String billamount) {

			Log.e("method called", "method called" + resid);

		 
			Intent intent = new Intent("com.google.zxing.client.android.SCAN");

			intent.putExtra("SCAN_MODE", "QR_CODE_MODE");

			startActivityForResult(intent, SCAN_BAR_CODE);

		}
		
		public void fbLogin()
		{
			
			Log.i("FACEBOOK LOGIN START","FACEBOOK LOGIN START");
		 
			
			 
			
	          
				
				
				  runOnUiThread(new Runnable() {

			           @Override
			           public void run() {
			                mFacebook.authorize(MainActivity.this, PERMS, new LoginDialogListener());
			                }
			            });           
		}

	}

	public String getDeviceId() {

		// final TelephonyManager tm = (TelephonyManager)
		// getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
		//
		// final String tmDevice, tmSerial, androidId;
		// tmDevice = "" + tm.getDeviceId();
		// tmSerial = "" + tm.getSimSerialNumber();
		// androidId = "" +
		// android.provider.Settings.Secure.getString(getContentResolver(),
		// android.provider.Settings.Secure.ANDROID_ID);
		//

		final String macAddr, androidId;

		WifiManager wifiMan = (WifiManager) this
				.getSystemService(Context.WIFI_SERVICE);
		WifiInfo wifiInf = wifiMan.getConnectionInfo();

		macAddr = wifiInf.getMacAddress();
		androidId = ""
				+ android.provider.Settings.Secure.getString(
						getContentResolver(),
						android.provider.Settings.Secure.ANDROID_ID);

		UUID deviceUuid = new UUID(androidId.hashCode(), macAddr.hashCode());
		// UUID deviceUuid = new UUID(androidId.hashCode(),
		// ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
		String deviceId = deviceUuid.toString();
		// String deviceId = Settings.System.getString(getContentResolver(),
		// Settings.System.ANDROID_ID);
		return deviceId;
	}

	public void sendPost(String url, File file) throws IOException,
			ClientProtocolException {

		url = "http://www.getmethemenu.ca/app/uploadphoto?resid=6026&userid=2&title="+ImageTitle+"&lat="
				+ currentLatitude
				+ "&lng="
				+ currentLongitude
				+ "&picture_taken_time=2011-07-17%2010:39:00";
		HttpClient httpclient = new DefaultHttpClient();
		httpclient.getParams().setParameter(
				CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

		HttpPost httppost = new HttpPost(url);
		// File file = new File(imagePath);

		MultipartEntity mpEntity = new MultipartEntity();
		ContentBody cbFile = new FileBody(file, "image/jpeg");
		mpEntity.addPart("image", cbFile);

		httppost.setEntity(mpEntity);
		Log.e("executing request " + httppost.getRequestLine(), "");
		HttpResponse response = httpclient.execute(httppost);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				response.getEntity().getContent(), "UTF-8"));
		String json = reader.readLine();
		HttpEntity resEntity = response.getEntity();

		Log.e("" + response.toString(), "");

//		Toast.makeText(getApplicationContext(), json + "", Toast.LENGTH_LONG)
//				.show();
		if (resEntity != null) {
			Log.e(EntityUtils.toString(resEntity), "");
		}
		if (resEntity != null) {
			resEntity.consumeContent();
		}
		httpclient.getConnectionManager().shutdown();
	}

	public void FindLocation() {
		LocationManager locationManager = (LocationManager) this
				.getSystemService(Context.LOCATION_SERVICE);

		LocationListener locationListener = new LocationListener() {
			public void onLocationChanged(Location location) {
				updateLocation(location);

				// Toast.makeText(
				// LocationFinder.this,
				// String.valueOf(currentLatitude) + "\n"
				// + String.valueOf(currentLongitude), 5000)
				// .show();

			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			public void onProviderEnabled(String provider) {
			}

			public void onProviderDisabled(String provider) {
			}
		};
		locationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);

	}

	void updateLocation(Location location) {
		currentLocation = location;
		currentLatitude = currentLocation.getLatitude();
		currentLongitude = currentLocation.getLongitude();
		// textView1.setText(String.valueOf(currentLatitude) + "\n"
		// + String.valueOf(currentLongitude));

	}

	public void onBackPressed() {
		if (webView.canGoBack())
			webView.goBack();
		else
			super.onBackPressed();
	}
	
	
	
	
	
	
	private class LoginDialogListener implements DialogListener {

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub
			final String token = mFacebook.getAccessToken();
			long token_expires = mFacebook.getAccessExpires();
			Log.d("FB", "AccessToken: " + token);
			Log.d("FB", "AccessExpires: " + token_expires);
			mProgressBar.setVisibility(ProgressBar.VISIBLE);
			mAsyncRunner.request("me", new SampleRequestListener());
		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

	}

	
	
	
	
	public class SampleRequestListener extends BaseRequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				// process the response here: executed in background thread
//				SharedPreference.putData(getApplicationContext(),
//						SharedPreference.FBRESPONSE, response);
				Log.d("Facebook-Example", "Response: " + response.toString());
				try {
					JSONObject json = Util.parseJson(response);

					Log.e("username", json.getString("username"));
//					SharedPreference.putData(getApplicationContext(),
//							SharedPreference.USERNAME,
//							json.getString("username"));
//					SharedPreference.putData(getApplicationContext(),
//							SharedPreference.EMAIL, json.getString("email"));
//					SharedPreference.putData(getApplicationContext(),
//							SharedPreference.ISLOGIN, "TRUE");
//					SharedPreference.putData(getApplicationContext(),
//							SharedPreference.ACCOUNT, "FACEBOOK");
//
//					RESPONSE = HttpHelper.postFBData(fburl, mFacebook
//							.getAccessToken(), SharedPreference.getData(
//							getApplicationContext(),
//							SharedPreference.FBRESPONSE), "facebook");
	//		Log.e("RESPONSE FROM GATEWAY", RESPONSE);
//
//					parsedata(RESPONSE);
					
					//http://www.getmethemenu.ca/app/facebook?email=visual.ps@gmail.com&uid=100002295358618&birthday=1984-06-17&gender=male&first_name=Pradeep&last_name=Shah&access_token=123456&devicetoken=123456
					
					email = json.getString("email");
					String childurl ="facebook?email="+json.getString("email")+"&uid="+json.getString("id")+"&birthday="+json.getString("birthday")+"&gender="+json.getString("gender")+"&first_name="+json.getString("first_name")+"&last_name="+json.getString("last_name")+"&access_token="+mFacebook.getAccessToken()+"&devicetoken="+getDeviceId();
					Log.e("PXR",BASE_API_URL+childurl);		
			String response1 =		Http.http_get(BASE_API_URL+childurl);
			Log.e("PXR",response1);	
					handler.sendEmptyMessage(2);
				} catch (FacebookError e) {
					// TODO Auto-generated catch block
					handler.sendEmptyMessage(1);
					e.printStackTrace();
				}

			} catch (Exception e) {
				// TODO: handle exception
				handler.sendEmptyMessage(1);
			}

		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			// TODO Auto-generated method stub

		}

	}
	
	 
	
}