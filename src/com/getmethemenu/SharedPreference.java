package com.getmethemenu;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {
	
	public static String USERNAME = "USERNAME";
	public static String ISLOGIN ="ISLOGIN";
	public static String ACCOUNT = "ACCOUNT";
	public static String EMAIL = "EMAIL";
	public static String FBRESPONSE ="FBRESPONSE";
	public static String TWITTERRESPONSE ="TWITTERRESPONSE";
	public static String CRITTERCISM ="CRITTERCISM";
	public static String APPTOKEN ="APPTOKEN";
	
	public static void putData(Context ct , String key , String value)
	{
		SharedPreferences myPrefs = ct.getSharedPreferences("myPrefs", 1);
        SharedPreferences.Editor prefsEditor = myPrefs.edit();
        prefsEditor.putString(key, value);
        prefsEditor.commit();
	}
	
	
	public static String getData(Context ct , String key)
	{
		SharedPreferences myPrefs = ct.getSharedPreferences("myPrefs", 1);
        String prefName = myPrefs.getString(key, "nothing");
        return prefName;
	}
	
	
	
	
	

}
